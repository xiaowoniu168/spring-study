package com.zsg.study.aop;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author: zsg
 * @description:
 * @date: 2020/4/11 23:39
 * @modified:
 */
@Configuration
@EnableAspectJAutoProxy
public class TestConfig {

	@Bean
	public Calculate calculate() {
		return new TestCalculate();
	}

	@Bean
	public TestLogAspect testLogAspect() {
		return new TestLogAspect();
	}
}
