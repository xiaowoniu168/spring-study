package com.zsg.study.aop;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author: zsg
 * @description:
 * @date: 2020/4/8 22:36
 * @modified:
 */
@ComponentScan("com.zsg.study.aop")
public class Test {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(Test.class);
		Calculate calculate = (Calculate) applicationContext.getBean("calculate");
		int add = calculate.add(1, 2);

	}
}
