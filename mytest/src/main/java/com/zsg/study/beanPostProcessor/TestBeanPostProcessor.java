package com.zsg.study.beanPostProcessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * @author: zsg
 * @description:
 * @date: 2020/1/4 20:50
 * @modified:
 */
@Component
public class TestBeanPostProcessor implements BeanPostProcessor {

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

		if (beanName.equals("beanPostProcessorServiceImpl")){
			System.out.println("Before");
		}

		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

		if (beanName.equals("beanPostProcessorServiceImpl")){
			System.out.println("After");
		}

		return bean;
	}
}
