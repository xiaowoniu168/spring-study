package com.zsg.study.beanPostProcessor;

import com.zsg.study.UserServiceImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author: zsg
 * @description:
 * @date: 2019/12/13 20:39
 * @modified:
 */
@ComponentScan("com.zsg.study.beanPostProcessor")
public class Test {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(Test.class);
		BeanPostProcessorServiceImpl userService = annotationConfigApplicationContext.getBean(BeanPostProcessorServiceImpl.class);
	}
}
