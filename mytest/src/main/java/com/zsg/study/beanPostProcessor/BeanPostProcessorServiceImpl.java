package com.zsg.study.beanPostProcessor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * @author: zsg
 * @description:
 * @date: 2020/1/4 20:52
 * @modified:
 */
@Service
public class BeanPostProcessorServiceImpl implements BeanPostProcessorService {

	@PostConstruct
	public void init(){
		System.out.println("init");
	}

	public BeanPostProcessorServiceImpl() {
		System.out.println("controstor");
	}
}
