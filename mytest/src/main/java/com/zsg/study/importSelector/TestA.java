package com.zsg.study.importSelector;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author: zsg
 * @description:
 * @date: 2020/4/1 22:15
 * @modified:
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface TestA {

	String value() default "aaa";
}
