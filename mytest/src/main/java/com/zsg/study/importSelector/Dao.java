package com.zsg.study.importSelector;

/**
 * @author: zsg
 * @description:
 * @date: 2020/2/12 20:41
 * @modified:
 */
public interface Dao {

	void query();
}
