package com.zsg.study.importSelector;

import com.zsg.study.UserServiceImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author: zsg
 * @description:
 * @date: 2019/12/13 20:39
 * @modified:
 */
@EnableZsg
@ComponentScan("com.zsg.study.importSelector")
public class Test {
	public static void main(String[] args) {

		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(Test.class);
		Dao indexDao = annotationConfigApplicationContext.getBean(Dao.class);
		indexDao.query();
	}
}
