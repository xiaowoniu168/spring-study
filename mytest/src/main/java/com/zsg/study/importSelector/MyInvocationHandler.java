package com.zsg.study.importSelector;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author: zsg
 * @description:
 * @date: 2020/2/12 20:42
 * @modified:
 */
public class MyInvocationHandler implements InvocationHandler {

	private Object target;

	public MyInvocationHandler(Object target) {
		this.target = target;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("我是代理");
		return method.invoke(target,args);
	}
}
