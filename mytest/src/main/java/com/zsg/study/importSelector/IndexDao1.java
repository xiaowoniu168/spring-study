package com.zsg.study.importSelector;

import org.springframework.stereotype.Component;

/**
 * @author: zsg
 * @description:
 * @date: 2020/2/12 20:41
 * @modified:
 */
@Component
public class IndexDao1 implements Dao {
	@Override
	public void query() {
		System.out.println("IndexDao1");
	}
}
