package com.zsg.study.importSelector;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author: zsg
 * @description:
 * @date: 2020/2/12 20:24
 * @modified:
 */
@Retention(RetentionPolicy.RUNTIME)
@Import(MyImportSeletor.class)
public @interface EnableZsg {
}
