package com.zsg.study.importSelector;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author: zsg
 * @description:
 * @date: 2020/4/1 23:24
 * @modified:
 */
@Component
public class Testc implements ApplicationListener<ContextRefreshedEvent> {
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		Map<String, Object> beansWithAnnotation = event.getApplicationContext().getBeansWithAnnotation(TestA.class);
	}
}
