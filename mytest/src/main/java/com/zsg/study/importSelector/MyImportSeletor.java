package com.zsg.study.importSelector;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.MethodMetadata;
import org.springframework.core.type.StandardAnnotationMetadata;

import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Set;

/**
 * @author: zsg
 * @description:
 * @date: 2020/2/12 20:17
 * @modified:
 */
public class MyImportSeletor implements ImportSelector {

	@Override
	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		Set<String> metaAnnotationTypes = importingClassMetadata.getMetaAnnotationTypes(TestA.class.getName());

//		Class<?> introspectedClass = ((StandardAnnotationMetadata) importingClassMetadata).getIntrospectedClass();
//		TestA annotation = introspectedClass.getAnnotation(TestA.class);
//		String value = annotation.value();
//		System.out.println(value);
//		String className = ((StandardAnnotationMetadata) importingClassMetadata).getClassName();
//		System.out.println(className);
		// 可以根据注解名称动态注入bean
//		boolean annotation = importingClassMetadata.hasAnnotation(EnableZsg.class.getName());
//		System.out.println(annotation);
		return new String[]{IndexDao.class.getName()};
	}
}
