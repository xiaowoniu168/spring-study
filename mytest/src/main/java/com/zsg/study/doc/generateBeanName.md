[https://docs.spring.io/spring-framework/docs/current/spring-framework-reference/core.html#beans-autowired-annotation-qualifiers][spring文档]


[https://www.cnblogs.com/jason0529/p/5272265.html][介绍]
# Spring源码入门——DefaultBeanNameGenerator解析
我们知道在spring中每个bean都要有一个id或者name标示每个唯一的bean，在xml中定义一个bean可以指定其id和name值，但那些没有指定的，或者注解的spring的beanname怎么来的的？就是BeanNameGenerator接口实现的特性。

```
　　<bean id="transactionManager" class="org.springframework.orm.jpa.JpaTransactionManager">
        <property name="entityManagerFactory" ref="entityManagerFactory" />
    </bean>
```

BeanNameGenerator接口位于 org.springframework.beans.factory.support 包下面，只声明了一个方法，接受两个参数：definition 被生成名字的BeanDefinition实例；registry 生成名字后注册进的BeanDefinitionRegistry。
```
/**
     * Generate a bean name for the given bean definition.
     * 根据给定的bean definition 生成一个bean的名字
     * @param definition the bean definition to generate a name for
     * @param 参数 definition 需要生成bean name的BeanDefinition实例
     * @param registry the bean definition registry that the given definition
     * is supposed to be registered with
     * @param 参数registry 是 definition 注册
     * @return the generated bean name
     * @return 返回生成的bean name
     */
    String generateBeanName(BeanDefinition definition, BeanDefinitionRegistry registry);
```