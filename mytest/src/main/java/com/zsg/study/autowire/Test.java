package com.zsg.study.autowire;

import com.zsg.study.UserServiceImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author: zsg
 * @description:
 * @date: 2019/12/13 20:39
 * @modified:
 */
@ComponentScan("com.zsg.study.autowire")
public class Test {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(Test.class);
//		IndexService indexService = annotationConfigApplicationContext.getBean(IndexService.class);
//		indexService.test();
		ApplicationContextAwareTest applicationContextAwareTest = annotationConfigApplicationContext.getBean(ApplicationContextAwareTest.class);
		applicationContextAwareTest.test();
	}
}
