package com.zsg.study.autowire;

/**
 * @author: zsg
 * @description:
 * @date: 2019/12/28 22:11
 * @modified:
 */
public interface UserDao {
	void test();
}
