package com.zsg.study.autowire;

import org.springframework.stereotype.Service;

/**
 * @author: zsg
 * @description:
 * @date: 2019/12/28 22:02
 * @modified:
 */
@Service
public class IndexDaoImpl implements IndexDao {

	@Override
	public void test() {
		System.out.println("IndexDaoImpl");
	}
}
