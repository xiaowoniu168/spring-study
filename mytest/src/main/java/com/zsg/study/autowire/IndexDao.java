package com.zsg.study.autowire;

/**
 * @author: zsg
 * @description:
 * @date: 2019/12/28 22:01
 * @modified:
 */
public interface IndexDao {
	void test();
}
