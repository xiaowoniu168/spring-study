package com.zsg.study.autowire;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * ApplicationContextAware 获取spring 容器 可以做到在任意地方获取到spring bean
 * 应用场景：
 * 一个单例的bean里面需要一个原型的bean可以这样做。
 * @author: zsg
 * @description:
 * @date: 2019/12/29 20:12
 * @modified:
 */
@Service
public class ApplicationContextAwareTest implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	public void test(){
		IndexService indexService = applicationContext.getBean(IndexService.class);
		indexService.test();
	}
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
