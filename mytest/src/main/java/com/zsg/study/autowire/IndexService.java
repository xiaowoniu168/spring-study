package com.zsg.study.autowire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 *
 * @Autowired 默认按照byType进行自动装配 如果匹配不上会按照byName进行匹配
 * @Resource 默认按照byName进行装配
 *    @Resource(type = IndexDaoImpl.class) 指定装配的bean
 * @author: zsg
 * @description:
 * @date: 2019/12/28 22:03
 * @modified:
 */
@Service
public class IndexService {

//	@Autowired
//	private IndexDao indexDaoImpl;
//
	@Autowired
	private UserDao aa;
//
//	@Autowired
//	private UserDao userDao;


	public void test(){
//		indexDaoImpl.test();

		aa.test();
//		userDao.test();
	}

//	public void setIndexDaoImpl1(IndexDao indexDao) {
//		this.indexDaoImpl1 = indexDao;
//	}
}
