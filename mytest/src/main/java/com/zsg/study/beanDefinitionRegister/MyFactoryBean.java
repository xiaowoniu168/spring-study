package com.zsg.study.beanDefinitionRegister;

import org.springframework.beans.factory.FactoryBean;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author zhangshuguang
 * @date 2020/02/06i
 */
public class MyFactoryBean<T> implements FactoryBean<T>, InvocationHandler {

	private Class<T> mapperInterface;

	public MyFactoryBean(Class<T> clazz) {
		this.mapperInterface = clazz;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("test");
		System.out.println(method.getName());
		return null;
	}

	@Override
	@SuppressWarnings({"unchecked", "rawtypes"})
	public T getObject() throws Exception {
		T o = (T) Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[]{mapperInterface}, this);
		return o;
	}

	@Override
	public Class<?> getObjectType() {
		return mapperInterface;
	}
}
