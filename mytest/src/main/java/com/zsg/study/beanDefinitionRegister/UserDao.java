package com.zsg.study.beanDefinitionRegister;

/**
 * @author zhangshuguang
 * @date 2020/02/06
 */
public interface UserDao {

	void query(String name);
}
