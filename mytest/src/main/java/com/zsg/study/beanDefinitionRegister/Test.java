package com.zsg.study.beanDefinitionRegister;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author: zsg
 * @description:
 * @date: 2019/12/13 20:39
 * @modified:
 */
@Configuration
@ComponentScan("com.zsg.study.beanDefinitionRegister")
@Import(MyImportBeanDefinitionRegister.class)
public class Test {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(Test.class);
		UserDao userDao = (UserDao) annotationConfigApplicationContext.getBean("userDao");
		userDao.query("aa");
	}
}
