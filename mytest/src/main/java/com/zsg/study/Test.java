package com.zsg.study;

import com.zsg.study.beanFactoryPostProcessor.MyBeanFactoryPostProcessor;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author: zsg
 * @description:
 * @date: 2019/12/13 20:39
 * @modified:
 */
@ComponentScan("com.zsg.study")
public class Test {
	public static void main(String[] args) {

		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(Test.class);
		UserServiceImpl userService1 = annotationConfigApplicationContext.getBean(UserServiceImpl.class);
		annotationConfigApplicationContext.refresh();
//		userService.test();
	}
}
