package com.zsg.study;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * @author: zsg
 * @description:
 * @date: 2019/12/13 20:28
 * @modified:
 */
@Service
public class UserServiceImpl implements UserService {

//	@Autowired
//	private UserService userService;

	@PostConstruct
	public void we(){
		System.out.println("PostConstruct1");
	}

	@PostConstruct
	public void sfdas(){
		System.out.println("PostConstruct2");
	}

	public UserServiceImpl() {
		System.out.println("constructor");
	}

	@PostConstruct
	public void init(){
		System.out.println("PostConstruct");
	}

	@PostConstruct
	public void aSDA(){
		System.out.println("PostConstruct3");
	}

	@PostConstruct
	public void safdsa(){
		System.out.println("PostConstruct4");
	}

	@Override
	public String test() {
		System.out.println("测试成功");
		return null;
	}
}
