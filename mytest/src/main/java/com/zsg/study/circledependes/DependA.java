package com.zsg.study.circledependes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: zsg
 * @description:
 * @date: 2020/4/6 19:11
 * @modified:
 */
@Component
public class DependA {

	private DependB dependB;

//	@Autowired
//	public DependA(DependB dependB) {
//		this.dependB = dependB;
//	}

	public DependB getDependB() {
		return dependB;
	}

	@Autowired
	public void setDependB(DependB dependB) {
		this.dependB = dependB;
	}
}
