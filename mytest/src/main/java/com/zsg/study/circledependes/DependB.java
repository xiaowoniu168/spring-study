package com.zsg.study.circledependes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: zsg
 * @description:
 * @date: 2020/4/6 19:11
 * @modified:
 */
@Component
public class DependB {

	private DependA dependA;

//	@Autowired
//	public DependB(DependA dependA) {
//		this.dependA = dependA;
//	}

	public DependA getDependA() {
		return dependA;
	}

	@Autowired
	public void setDependA(DependA dependA) {
		this.dependA = dependA;
	}
}
